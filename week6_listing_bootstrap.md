This tut helps you to arrange listing (product catalogue) similarly to Tiki, or in other word, there are multiple books per row and multiple rows. People call this grid layout.

## Prepare
  1. You have scaffolded `Book` controllers and all related stuffs.
  2. You created many books (about 5 is enough)

## Main
  1. Copy and rename the `Index` action (also its view) to `List`
  2. Open the List view (List.cshtml) and change it to this
  
  ```html
    @model List<Demo6.Models.Book>

    @{
        ViewData["Title"] = "Index";
    }

    <h1>List</h1>

    <div class="container">
        <div class="row">
            @for (var i = 0; i < @Model.Count; i++)
            {
                <div class="col-md-4 col-6">
                    <div class="card" style="width: 18rem;">
                        <img src="" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">@Model[i].Title</h5>
                            <a href="#" class="btn btn-primary">Detail</a>
                            <a asp-action="AddToCart" asp-route-isbn="@Model[i].Isbn" class="btn btn-primary">Add to cart</a>
                        </div>
                    </div>
                </div>
            }
        </div>
    </div>
  ```

## Explain:
  1. (Note!) You can use `foreach` instead of `for`
  2. I use [Grid](https://getbootstrap.com/docs/5.1/layout/grid/) layout by `class="container"` and `class="row">`
  3. `<div class="col-md-4 col-6">` means each book (item) has 4/12 width space on *Medium* size screen, or in other word, 3 books each row in *Medium* size screen. Similarly, `col-6` mean each book takes place of 6/12 width on `Small` screen, thus 2 books each row in `Small` screen shown.
  4. `<div class="card"` is for [Card](https://getbootstrap.com/docs/5.1/components/card/) style (similar to Tiki product) for showing each item (each book).