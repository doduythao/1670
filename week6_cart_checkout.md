# Cart, Add to cart, Checkout
I'm lazy now at the holiday so I post a very simple, plain, **incomplete** solution here. You guy should make it better by handling the exception and many other stuffs.

**Do not just copy and paste my work. Violating leads to a failure of this course**

*Report bugs and fix problem by raising issue is highly welcomed.*

## Create Cart entity, Modify related entities, Add it to the main DbContext.

  1. Create Cart entity

```c#
    public class Cart
    {
        public string UId { get; set; }
        public string BookIsbn { get; set; }
        public AppUser? User { get; set; }
        public Book? Book { get; set; }
    }
```

  2. Modify related entities

```c#
public class AppUser : IdentityUser
{
    public DateTime? DoB { get; set; }
    public string? Address { get; set; }
    public Store? Store { get; set; }
    public virtual ICollection<Order>? Orders { get; set; }
    public virtual ICollection<Cart>? Carts { get; set; }
}
```

```c#
public class Book
{
    //OLD code
    public virtual ICollection<Cart>? Carts { get; set; }
}
```
  3. Add and Configure Cart into your DbContext *like* this

```c#
public class UserContext : IdentityDbContext<AppUser>
{
    public UserContext(DbContextOptions<UserContext> options)
        : base(options)
    {
    }
    //OLD code
    public DbSet<Cart> Cart { get; set; }
    protected override void OnModelCreating(ModelBuilder builder)
    {
        //OLD Code
        builder.Entity<Cart>()
            .HasKey(c => new { c.UId, c.BookIsbn });
        builder.Entity<Cart>()
            .HasOne<AppUser>(c => c.User)
            .WithMany(u => u.Carts)
            .HasForeignKey(c => c.UId);
        builder.Entity<Cart>()
            .HasOne<Book>(od => od.Book)
            .WithMany(b => b.Carts)
            .HasForeignKey(od => od.BookIsbn)
            .OnDelete(DeleteBehavior.NoAction);
    }
}
```

  4. Add migration and update db correspondingly!

## Make Cart controller and its Index view.

```c#
public class CartController : Controller
{
    private readonly UserContext _context;
    private readonly UserManager<AppUser> _userManager;

    public CartController(UserContext context, UserManager<AppUser> userManager)
    {
        _context = context;
        _userManager = userManager;
    }

    public ActionResult Index()
    {
        string thisUserId = _userManager.GetUserId(HttpContext.User);
        return View(_context.Cart.Where(c => c.UId == thisUserId));
    }
}
```

```html
@model IEnumerable<Demo6.Models.Cart>

@{
	ViewData["Title"] = "Index";
}

<h1>Index</h1>

<table class="table">
	<thead>
		<tr>
			<th>
				@Html.DisplayNameFor(model => model.UId)
			</th>
			<th>
				@Html.DisplayNameFor(model => model.BookIsbn)
			</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		@foreach (var item in Model)
		{
			<tr>
				<td>
					@Html.DisplayFor(modelItem => item.UId)
				</td>
				<td>
					@Html.DisplayFor(modelItem => item.BookIsbn)
				</td>
			</tr>
		}
	</tbody>
</table>

<a asp-controller="Books" asp-action="Checkout">Checkout</a>
```

## Add button `Add to cart` to `List` view of Books like below

```html
<div class="card" style="width: 18rem;">
    <img src="" class="card-img-top" alt="...">
    <div class="card-body">
        <h5 class="card-title">@Model[i].Title</h5>
        <a href="#" class="btn btn-primary">Detail</a>
        <a asp-action="AddToCart" asp-route-isbn="@Model[i].Isbn" class="btn btn-primary">Add to cart</a>
    </div>
</div>
```

## Add `AddToCart` and `Checkout` actions to `Book` controller

```c#
public async Task<IActionResult> AddToCart(string isbn)
{
    string thisUserId = _userManager.GetUserId(HttpContext.User);
    Cart myCart = new Cart() { UId = thisUserId, BookIsbn = isbn };
    Cart fromDb = _context.Cart.FirstOrDefault(c => c.UId == thisUserId && c.BookIsbn == isbn);
    //if not existing (or null), add it to cart. If already added to Cart before, ignore it.
    if (fromDb == null)
    {
        _context.Add(myCart);
        await _context.SaveChangesAsync();
    }
    return RedirectToAction("List");
}
```

```c#
public async Task<IActionResult> Checkout()
{
    string thisUserId = _userManager.GetUserId(HttpContext.User);
    List<Cart> myDetailsInCart = await _context.Cart
        .Where(c => c.UId == thisUserId)
        .Include(c => c.Book)
        .ToListAsync();
    using (var transaction = _context.Database.BeginTransaction())
    {
        try
        {
            //Step 1: create an order
            Order myOrder = new Order();
            myOrder.UId = thisUserId;
            myOrder.OrderDate = DateTime.Now;
            myOrder.Total = myDetailsInCart.Select(c => c.Book.Price)
                .Aggregate((c1, c2) => c1 + c2);
            _context.Add(myOrder);
            await _context.SaveChangesAsync();

            //Step 2: insert all order details by var "myDetailsInCart"
            foreach (var item in myDetailsInCart)
            {
                OrderDetail detail = new OrderDetail()
                {
                    OrderId = myOrder.Id,
                    BookIsbn = item.BookIsbn,
                    Quantity = 1
                };
                _context.Add(detail);
            }
            await _context.SaveChangesAsync();

            //Step 3: empty/delete the cart we just done for thisUser
            _context.Cart.RemoveRange(myDetailsInCart);
            await _context.SaveChangesAsync();
            transaction.Commit();
        }
        catch (DbUpdateException ex)
        {
            transaction.Rollback();
            Console.WriteLine("Error occurred in Checkout" + ex);
        }
    }
    return RedirectToAction("Index", "Cart");
}
```