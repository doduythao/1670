# How to send mail using Google (easy way) personal account.
  - *If you don't wanna use/enable email verification* (i.e. you don't wanna force register users to open their mailbox to get confirmation code or link), then you don't have to configure this email sending.
  - This is a *feature* to support register in Identity (Authentication)

## Note before reading this.
  - if you already read, try the Vietnamese docs in the last section of `week4_identity_basic.md` then you already know how to do this. Stop the learning here.
  - if you read it but still confused and don't know how to do it. Then try this tutorial.
  - [**Source of this tutorial here** ](https://jasonwatmore.com/post/2022/03/11/net-6-send-an-email-via-smtp-with-mailkit)

## Prepare:
  1. Install this `Install-Package MailKit` using `PMC`

## Main steps:
  2. Prepare an email by this below line of code. Insert it in where you wanna to send an email, like *every time click to **About** page of **Home** controller*
  ```c#
    var email = new MimeMessage();
    email.From.Add(MailboxAddress.Parse("from_address@example.com"));
    email.To.Add(MailboxAddress.Parse("to_address@example.com"));
    email.Subject = "Test Email Subject";
    email.Body = new TextPart(TextFormat.Plain) { Text = "Example Plain Text Message Body" };
  ```

  3. Please change the `to_address@example.com` to `<your real target email address>` (*where you could test/open*)

  4. Send the composed email by an SmtpClient object (configured it in `appsetting.json` or directly where you use it, up to you) like this code below:
  ```c#
    using var smtp = new SmtpClient();
    smtp.Connect("smtp.ethereal.email", 587, SecureSocketOptions.StartTls);
    smtp.Authenticate("[USERNAME]", "[PASSWORD]");
    smtp.Send(email);
    smtp.Disconnect(true);
  ```
  - *Please* change `[USERNAME]`, `[PASSWORD]`, `smtp.ethereal.email` and port `587` to some **real input**. 
  - (4.2) *How* to get **real input** ? Visit this [website](https://ethereal.email/) > Click button `+ Create Ethereal Account`
  - (4.3) You have `Username`, `Password`, `host`, `port` to fill the code above. *Don't close this tab.*

  5. Run the code and open `<your real target email address>` (in step 2.) to see if there's email coming (Testing)
  - *Check* also junk box/trash box/spam box
  - IF YOU USE `ethereal.email`, THEY WON'T ACTUALLY SEND EMAIL TO THE TARGET ADDRESS. GET BACK TO (4.3) TAB, click `Open Mailbox` TO SEE ALL YOUR SENT MAILS.

## Configure to Gmail account (instead of Ethereal)
  1. Login your gmail (if not yet)
  2. Go to this: https://myaccount.google.com/lesssecureapps
  3. Turn this on (blue/green color) not grey
  4. Then you gonna fill **Main steps/(4.2)** by below info.
  ```
    Gmail SMTP server address: smtp.gmail.com
    Gmail SMTP name: Your google shown name
    Gmail SMTP username: Your full Gmail address (e.g. you@gmail.com)
    Gmail SMTP password: The password that you use to log in or app password
    Gmail SMTP port (TLS): 587
  ```
  5. Run the code and test if you received the email from Gmail account.

## Bring Smtp Config Info to `appsettting.json` and use Service Injection to use email sender (better way)

### I. Prepare EmailSenderOptions and EmailSender class
  1. Create folder named `Email` on the same par with `Controllers`, `Models`
  2. In this folder, create class `EmailSenderOptions.cs`

  ```c#
  using MailKit.Security;
  public class EmailSenderOptions
  {
      public EmailSenderOptions()
      {
          Host_SecureSocketOptions = SecureSocketOptions.Auto;
      }

      public string Host { get; set; }
      public int Port { get; set; }

      public string User { get; set; }
      public string Pass { get; set; }

      public string Sender { get; set; }
      public string Name { get; set; }
      public SecureSocketOptions Host_SecureSocketOptions { get; set; }
  }
  ```
  3. And create class `EmailSender.cs`
  ```c#
  using MailKit.Net.Smtp;
  using Microsoft.AspNetCore.Identity.UI.Services;
  using Microsoft.Extensions.Options;
  using MimeKit;
  using MimeKit.Text;

  public class EmailSender : IEmailSender
  {
      public EmailSender(IOptions<EmailSenderOptions> options)
      {
          this.Options = options.Value;
      }

      public EmailSenderOptions Options { get; set; }

      public Task SendEmailAsync(string email, string subject, string message)
      {
          return Execute(email, subject, message);
      }

      public Task Execute(string to, string subject, string message)
      {
          // create message
          var email = new MimeMessage();
          email.Sender = MailboxAddress.Parse(Options.Sender);
          if (!string.IsNullOrEmpty(Options.Name))
              email.Sender.Name = Options.Name;
          email.From.Add(email.Sender);
          email.To.Add(MailboxAddress.Parse(to));
          email.Subject = subject;
          email.Body = new TextPart(TextFormat.Html) { Text = message };

          // send email
          using (var smtp = new SmtpClient())
          {
              smtp.Connect(Options.Host, Options.Port, Options.Host_SecureSocketOptions);
              smtp.Authenticate(Options.User, Options.Pass);
              smtp.Send(email);
              smtp.Disconnect(true);
          }

          return Task.FromResult(true);
      }
  }
  ```
### II. Add EmailSender as a service to `Program.cs`
  1. Add below code BEFORE this line `var app = builder.Build();` in `Program.cs`
  ```c#
  var config = builder.Configuration;
  builder.Services.AddTransient<IEmailSender, EmailSender>();
  builder.Services.Configure<EmailSenderOptions>(options =>
  {
      options.Host = config["MailSettings:Host"];
      options.Port = int.Parse(config["MailSettings:Port"]);
      options.User = config["MailSettings:User"];
      options.Pass = config["MailSettings:Pass"];
      options.Name = config["MailSettings:Name"];
      options.Sender = config["MailSettings:User"];
  });
  ```
  2. Bring smtp config info to `appsettting.json` by edit `appsetting.json` *LIKE* this
  ```json
  {
      "Logging": {
          "LogLevel": {
              "Default": "Information",
              "Microsoft.AspNetCore": "Warning"
          }
      },
      "AllowedHosts": "*",
      "ConnectionStrings": {
          "UsersContextConnection": "Server=(localdb)\\mssqllocaldb;Database=week2;Trusted_Connection=True;MultipleActiveResultSets=true"
      },
      "MailSettings": {
          "User": "joaquin.little48@ethereal.email",
          "Name": "Joaquin Little",
          "Pass": "UsyMJGuYvwGZ7rzdAy",
          "Host": "smtp.ethereal.email",
          "Port": 587
      }
  }
  ```
### III. Use the registered EmailSender service to send email.
  1. Test again by try `Register` feature of `week4_identity_basic.md` lesson to see if an email was really sent with an activation url inside.

  2. Another test, also **how to inject service and use it**
  
  - Edit the `Home` controller to look *LIKE* this

  ```c#
    private readonly IEmailSender _emailSender;
    public HomeController(ILogger<HomeController> logger, IEmailSender emailSender)
    {
        _logger = logger;
        _emailSender = emailSender;
    }
  ```
  - And change the `Privacy` action to this:
  ```c#
    public async Task<IActionResult> Privacy()
    {
        await _emailSender.SendEmailAsync("thaodo@gmail.com", "test send mail", "just test");
        return View();
    }
  ```


## Learn more in Vietnamese tutorial:
  - Try this [tutorial](https://xuanthulab.net/asp-net-core-gui-mail-trong-ung-dung-web-asp-net.html)