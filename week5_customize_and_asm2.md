*This tutorial would help you to know how to extend the IdentityUser and use this User to be a part of your web application.*

*Note that this is very basic and simple tutorial to demo, don't copy paste the whole thing to your assignment, it's not enough for the requirements*

We gonna add **2 fields** (`Date of Birth` and `Address`) to the IdentityUser and we gonna have **4 tables/entities** connected to each other to form a simple application.
  - Store
  - Book
  - Order
  - OrderDetail

## Relationship type between entities

I assume the relationships like below. You could design it in a different way, as long as it's reasonable and not too impractical.
  
  - Store **1:1** AppUser (This extended from IdentityUser)
  - Store **1:N** Book
  - Order **N:1** AppUser
  - Order **1:N** OrderDetail
  - Book  **1:N** OrderDetail

## Prepare steps:
  1. Create an MVC project, no authentication default!
  2. Scaffold Identity component like in `week4_identity_basic.md` (Only `Prepare` and `I. Main steps`)

## Main steps:
  Your project structure would look like this in general
  ![demo 6 structure](images/demo6_1.png)

  1. `AppUser.cs`
  ```c#
    using Demo6.Models;
    using Microsoft.AspNetCore.Identity;

    namespace Demo6.Areas.Identity.Data;

    // Add profile data for application users by adding properties to the AppUser class
    public class AppUser : IdentityUser
    {
        public DateTime? DoB { get; set; }
        public string? Address { get; set; }
        public Store? Store { get; set; }
        public virtual ICollection<Order>? Orders { get; set; }
    }
  ```

  2. `Store.cs`
  ```c#
    using Demo6.Areas.Identity.Data;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    namespace Demo6.Models
    {
        public class Store
        {
            [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public int Id { get; set; }
            public string Name { get; set; }
            public string Address { get; set; }
            public string Slogan { get; set; }
            public string UId { get; set; }
            public AppUser User { get; set; }
            public virtual ICollection<Book>? Books { get; set; }
        }
    }
  ```

  3. `Book.cs`
  ```c#
    using System.ComponentModel.DataAnnotations;

    namespace Demo6.Models
    {
        public class Book
        {
            [Key]
            public string Isbn { get; set; }
            public string Title { get; set; }
            public int Pages { get; set; }
            public string Author { get; set; }
            public string Category { get; set; }
            public double Price { get; set; }
            public string Desc { get; set; }
            public string ImgUrl { get; set; }
            public int StoreId { get; set; }
            public Store Store { get; set; }
            public virtual ICollection<OrderDetail>? OrderDetails { get; set; }
        }
    }
  ```

  4. `Order.cs`
  ```c#
    using Demo6.Areas.Identity.Data;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    namespace Demo6.Models
    {
        public class Order
        {
            [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public int Id { get; set; }
            public string UId { get; set; }
            public DateTime OrderDate { get; set; }
            public double Total { get; set; }
            public AppUser User { get; set; }
            public virtual ICollection<OrderDetail>? OrderDetails { get; set; }
        }
    }
  ```

  5. `OrderDetail.cs`
  ```c#
    namespace Demo6.Models
    {
        public class OrderDetail
        {
            public int OrderId { get; set; }
            public string BookIsbn { get; set; }
            public int Quantity { get; set; }
            public Order Order { get; set; }
            public Book Book { get; set; }
        }
    }
  ```

  6. `UserContext.cs`
  ```c#
    using Demo6.Areas.Identity.Data;
    using Demo6.Models;
    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore;

    namespace Demo6.Data;

    public class UserContext : IdentityDbContext<AppUser>
    {
        public UserContext(DbContextOptions<UserContext> options)
            : base(options)
        {
        }
        public DbSet<Store> Store { get; set; }
        public DbSet<Book> Book { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<OrderDetail> OrderDetail { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<AppUser>()
                .HasOne<Store>(au => au.Store)
                .WithOne(st => st.User)
                .HasForeignKey<Store>(st => st.UId);

            builder.Entity<Book>()
                .HasOne<Store>(b => b.Store)
                .WithMany(st => st.Books)
                .HasForeignKey(b => b.StoreId);

            builder.Entity<Order>()
                .HasOne<AppUser>(o => o.User)
                .WithMany(ap => ap.Orders)
                .HasForeignKey(o => o.UId);

            builder.Entity<OrderDetail>()
                .HasKey(od => new { od.OrderId, od.BookIsbn });
            builder.Entity<OrderDetail>()
                .HasOne<Order>(od => od.Order)
                .WithMany(or => or.OrderDetails)
                .HasForeignKey(od => od.OrderId);
            builder.Entity<OrderDetail>()
                .HasOne<Book>(od => od.Book)
                .WithMany(b => b.OrderDetails)
                .HasForeignKey(od => od.BookIsbn);
        }
    }

  ```

  7. Run `Add-Migration CreateBookSchema` in PMC (with specifying the context or not up to you)

  8. Open the Migration plan file (like `20220420043528_CreateBookSchema.cs`) under `Migration` folder, find and edit this area to look like mine

  ![fix multiple cascase](/images/demo6_2.png)

  9. `Update-Database` in PMC

## Review, QnA
  1. WTH is this `[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]` ?
  
  > These similar things are called "Data Annotations", learn more example at [here](https://docs.microsoft.com/en-us/ef/core/modeling/keys?tabs=data-annotations). In this particular line, it is set the next column to primary key with auto incremental.

  2. What is this `.HasOne<Store>(au => au.Store)` ?
  
  > This is what people call `Fluent API`, learn more at [here](https://www.entityframeworktutorial.net/efcore/configure-one-to-many-relationship-using-fluent-api-in-ef-core.aspx). Why ? to describe foreign key, constraints and many other configuration?. Note at `UserContext.cs`, focus on 1:1 relation, 1:N relation and learn why I wrote those code.

  3. Why we need step `8.` ? 
  > SQL doesn't allow multiple cascase paths(short answer). And any consequence if no delete cascade ? Just be careful in deleting a book or an order (or another way is just mark the order *cancel* state or mark a book *out-of-service* state)

  4. Why we need `virtual` for collection ? 
  > to support lazy load in EF, system only load when you use it, reduce loading (short answer)

  5. I highly recommend to use **explicitly** way to describe foreign key and relationship by `Fluent API` and avoid using implicit convention in naming fields since we are not sure what happen in case of exceptions and sometimes their names sound silly or not really human-friendly or "make-sense".

  6. There is a thing called `chaining` in `Fluent API`, that why you sometimes *cannot* set primary keys and foreign key at the same time in one time using `Fluent API`. We can break tasks to *more than one* line of code similar to my snipet
  
  ```c#
    builder.Entity<OrderDetail>()
        .HasKey(od => new { od.OrderId, od.BookIsbn });
    builder.Entity<OrderDetail>()
        .HasOne<Order>(od => od.Order)
        .WithMany(or => or.OrderDetails)
        .HasForeignKey(od => od.OrderId);
    builder.Entity<OrderDetail>()
        .HasOne<Book>(od => od.Book)
        .WithMany(b => b.OrderDetails)
        .HasForeignKey(od => od.BookIsbn);
  ```

  