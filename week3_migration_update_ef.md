# Migration/Update DB from (to) DB to (from) Codebase.

## 1. Migration to DB from Code classes (Code-first)

*Use it in case `context.Database.EnsureCreated();` got error or you wanna update the code base while preserving data existing in DB*

### Prepare steps:

  - Define Model entity classes in `Models` folder.
  - Install using Package Manager Console (`PMC`) 3 packages:
  ```powershell
  Install-Package Microsoft.EntityFrameworkCore
  Install-Package Microsoft.EntityFrameworkCore.Tools
  Install-Package Microsoft.EntityFrameworkCore.SqlServer
  ```
  - Create `<YourContext>` based on `DbContext` in `Data` folder.
  - Add `connection string` to `appsetting.json`.
  - Register `<YourContext>` to `builder.Services` in `Program.cs`
  

### Main steps:

  - `Add-Migration <name_a_migration_with_version>` to create a Migration scenario (could be DDL for non-existing tables, could be alter for existing table, could be inserted if you had DbInitalizer and so on)
    - This command create a *snapshot* and a *migration*. Check *migration* file if `PMC` generates correct or proper DDL (i.e. check PK, FK, auto incremental and so on) in `Up` & `Down` methods. 
    - If not, modify to what we really want.
  - `Update-Database` to process the migration scenario to implement/run on DB finally.

## 2. Migration from DB (DB-first approach) to Code classes.

### Preparation:

  - Install 3 Entity Framework packages using `PMC`

  ```powershell
  Install-Package Microsoft.EntityFrameworkCore
  Install-Package Microsoft.EntityFrameworkCore.Tools
  Install-Package Microsoft.EntityFrameworkCore.SqlServer
  ```

  - Create a database with DDL script, such as a simple Store below, (no need sample data):

  ```sql
  create database mystore;
  use mystore;

  create table users
  (id int identity primary key,
  firstname nvarchar(20),
  DoB date);

  create table orders
  (order_id int identity primary key,
  u_id int foreign key references users(id),
  order_date date);

  create table products
  (prod_id int identity primary key,
  prod_name nvarchar(20));

  create table order_details
  (order_id int foreign key references orders(order_id),
  product_id int foreign key references products(prod_id),
  quantity int,
  primary key(order_id,product_id));
  ```

### Main steps:

  - Run Scaffolding to create classes and DbContext in the `Models` folder.

  ```powershell
  Scaffold-DbContext "Server=(localdb)\mssqllocaldb;Database=mystore;Trusted_Connection=True;MultipleActiveResultSets=true" Microsoft.EntityFrameworkCore.SqlServer -OutputDir Models
  ```

  - Correct or fix the just generated DbContext (remove DB user/password for example) and all classess as you wish.
  - Refactoring DbContext to `Data` folder (don't forget change the namespace and using and so on)
  - (Optional) add connnection string to `appsetting.json` and register service in `Program.cs` to prepare for scaffolding.
