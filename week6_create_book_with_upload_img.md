This tutorial helps you to create book with *uploading image file* feature and automatically **map the book's store to the current logged user's store.**

## Prepare:
  1. Scaffold `Store` controller and views.
  2. Register at least 1 account if not available.
  3. Create at least 1 Store with existing account.
  4. Scaffold `Book` controller and views.

## Main:
  1. Change the form in `Create` view of the `Book` controller to similar to this

  ```html
  <form asp-action="Create" enctype="multipart/form-data">
  ```
  
  2. Keep going with the form, replace the `ImgUrl` area by this
  
  ```html
    <div class="form-group">
        <label class="control-label">Image</label>
        <input type="file" class="form-control" name="image"/>
    </div>
  ```

  3. Keep going with the form, remove the area of `StoreId` since we automatically map it to the current logged user's store in backend side.

  4. Go to `Book` controller, add `_userManager` first (similar to below code).

  ```c#
    public class BooksController : Controller
    {
        private readonly UserContext _context;
        private readonly UserManager<AppUser> _userManager;

        public BooksController(UserContext context, UserManager<AppUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }
  ```

  5. Keep going in the `Book` controller, replace the `Create` (HttpPost) with below snippet

  ```c#
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("Isbn,Title,Pages,Author,Category,Price,Desc")] Book book, IFormFile image)
    {
        if (image != null)
        {
            string imgName = book.Isbn + Path.GetExtension(image.FileName);
            string savePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img", imgName);
            using (var stream = new FileStream(savePath, FileMode.Create))
            {
                image.CopyTo(stream);
            }
            book.ImgUrl = "img/" + imgName;

            var thisUserId = _userManager.GetUserId(HttpContext.User);
            Store thisStore = await _context.Store.FirstOrDefaultAsync(s => s.UId == thisUserId);
            book.StoreId = thisStore.Id;
        }
        else
        {
            return View(book);
        }
        _context.Add(book);
        await _context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
    }
  ```

  6. Run the web, login, and test the `Create` action of Book


