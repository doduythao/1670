# Scaffold basic identity (and prepare for authorization)

## Basic Scaffold on an existing MVC project (những dự án MVC có sẵn như ở tuần 2)
### Prepare: 
  - Project có sẵn như ở tuần 2 là được. *Miễn là lúc tạo project ko bật Authentication (None)*
  - Install this package
  ```powershell
  Install-Package Microsoft.VisualStudio.Web.CodeGeneration.Design
  ```

### I. Main steps:
  1. From Solution Explorer, right-click on the project > Add > New Scaffolded Item.
  2. From the left pane of the Add New Scaffolded Item dialog, select Identity. Select Identity in the center pane. Select the Add button.
  3. In the Add Identity dialog, select `Login`, `Logout`, `Register`
  4. (Avoid using an existing Context like SchoolContext) Create a new Data Context like `UserContext` by click (+)
  5. Also Add new User class by click (+), name it by yourself (like `week4User`)
  6. Run scaffolding.

### II. Next main steps:
  7. Install below packages from `PMC`
  ```powershell
    Install-Package Microsoft.EntityFrameworkCore
    Install-Package Microsoft.EntityFrameworkCore.Tools
    Install-Package Microsoft.EntityFrameworkCore.SqlServer
  ```
  8. `Add-Migration CreateIdentitySchema -Context UserContext` to create plan to migrate to DB
  9. *(Optional now but very important later)* check the **connection string** of the whole identity system schema (the authentication system is based on DB!). Leave the current DB or replace by an existing DB (up to you).
  10. `Update-Database -Context UserContext` to really commit (create database or tables) to DB finally.

### III. Complete Views and all:
  11. Add this code `<partial name="_LoginPartial" />` after `</ul>` in navigation area in `Views/Shared/_Layout.cshtml`
  12. Add this code `app.MapRazorPages();` after the below code:
  ```c#
  app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");
  ```

### IV. Test 
  1. Run the app
  2. Click Register
  3. Fill the register form
  4. Confirm registration by click the link
  5. Login with your filled info (what you filled in step 3.)
  6. If it ok, all steps are ok, you should have **Hello YourEmail@...** greeting at the right corner.

### V. Basic Configuration
  1. Open `Program.cs`
  2. Refactoring the generated code from scaffolding (auto generated) by this code. (fix the class names to your case)
  
  ```c#
  builder.Services.AddDbContext<UsersContext>(options =>
      options.UseSqlServer(builder.Configuration.GetConnectionString("UsersContextConnection")));

  builder.Services.AddDefaultIdentity<week4User>(options => options.SignIn.RequireConfirmedAccount = true)
      .AddEntityFrameworkStores<UsersContext>();
  ```

  3. Configure a bit on Identity Option, try the code below (add it after above code)
  ```c#
  builder.Services.Configure<IdentityOptions>(options =>
  {
      // Password settings.
      options.Password.RequireDigit = true; //Password bắt buộc có số ?
      options.Password.RequireLowercase = true; //Pass bắt buộc có chữ thường
      options.Password.RequireNonAlphanumeric = true; //Tương tự, tự google
      options.Password.RequireUppercase = true;
      options.Password.RequiredLength = 6;    //Độ dài ít nhất cũng phải >= 6 kí tự.

      // User settings.
      options.User.AllowedUserNameCharacters =
      "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._@";
      options.User.RequireUniqueEmail = false;
  });
  ```
  4. Configure a bit on Cookies (add it after step 3. )
  ```c#
  builder.Services.ConfigureApplicationCookie(options =>
  {
      // Cookie settings
      options.Cookie.HttpOnly = true; 
      options.ExpireTimeSpan = TimeSpan.FromMinutes(10);  //thời gian cookie hiệu lực
      options.SlidingExpiration = true;
  });
  ```

## Vietnamese Learn-more Document on Identity (highly recommended):
*Check this post if you wanna learn more and deeper in Vietnamese, it's a bit lengthy, code written for version 5 and he uses .Net CLI, not PMC like what I taught*

[Xem thêm ở đây bằng tiếng Việt](https://xuanthulab.net/asp-net-razor-su-dung-identity-de-tao-user-xac-thuc-dang-nhap-website.html)
