# Authorization: Add role to user and more.

## Add (Create) roles and Map (add) an existing role to created user.
### Why we need this ?
Assume you need 2 Roles: `Customer` and `Store-Owner`. 

You use **different dashboards for two roles**, e.g. *Customer can access CustomerDashboard page but cannot view OwnerDashboard page and vice versa*. 

### Preparation
  - Created project MVC .Net Core 6. as usual.
  - Completed the `week4_identity_basic.md` (just step I, II, III; no need step V)

### I. Main Steps
  1. Open `Program.cs` at the top area, refactor your code to **similar** to below code.
  ```c#
    var builder = WebApplication.CreateBuilder(args);
    builder.Services.AddDbContext<UsersContext>(options =>
        options.UseSqlServer(builder.Configuration.GetConnectionString("UsersContextConnection")));

    builder.Services.AddDefaultIdentity<week2User>(options => options.SignIn.RequireConfirmedAccount = true)
        .AddRoles<IdentityRole>()
        .AddEntityFrameworkStores<UsersContext>();
  ```

  2. Add below code to *after* the line `var app = builder.Build();`
  ```c#
    using (var scope = app.Services.CreateScope())
    {
        var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();
        string[] roleNames = { "Customer", "Seller" };
        IdentityResult roleResult;
        foreach (var roleName in roleNames)
        {
            var roleExist = await roleManager.RoleExistsAsync(roleName);
            if (!roleExist)
            {
                roleResult = await roleManager.CreateAsync(new IdentityRole(roleName));
            }
        }
    }
  ```

  3. Open file at `Areas/Identity/Pages/Account/Register.cshtml/Register.cshtml.cs`

  4. From this file, add below code *after* `private readonly ...` block
  ```c#
    public List<SelectListItem> Roles { get; } = new List<SelectListItem>
        {
            new SelectListItem { Value = "Customer", Text = "Customer" },
            new SelectListItem { Value = "Seller", Text = "Store Owner" }
        };
  ```

  5. Also in this file, find `public class InputModel` area, add below code *after* line `public string ConfirmPassword { get; set; }`

  ```c#
    [Required]
    [Display(Name = "Your Role")]
    public string Role { get; set; }
  ```

  6. Same file, find `OnPostAsync` action, then find `if (result.Succeeded)` area, add below code *after* `_logger.LogInformation("User created a new account with password.");` line

  ```c#
  await _userManager.AddToRoleAsync(user, Input.Role);    //Add chosen role to new created user
  ```

  7. Change the View of Register, open file `Areas/Identity/Pages/Account/Register.cshtml`, add below code to the form **before** *password* input
  ```html
    <div class="form-floating">
        <select asp-for="Input.Role" asp-items="Model.Roles" class="form-control"></select>
        <label asp-for="Input.Role"></label>
    </div>
  ```

### II. Test Register
  - Test by running the project, check if any error.
  - (**II.1**) Try to Register a new user with Role = Customer, activate it (by mail), remember pass.
  - (**II.2**) Try to Register a new user with Role = Sellers, activate it (by mail), remember pass.

## Demo Authorization
  1. Open `HomeController.cs`, refactor the top area to *similar* to this
  ```c#
    private readonly ILogger<HomeController> _logger;
    private readonly IEmailSender _emailSender;
    private readonly UserManager<week2User> _userManager;

    public HomeController(ILogger<HomeController> logger, IEmailSender emailSender, UserManager<week2User> userManager)
    {
        _logger = logger;
        _emailSender = emailSender;
        _userManager = userManager;
    }
  ```

  2. Add 2 Action by below code.
  ```c#
    [Authorize(Roles = "Customer")]
    public IActionResult ForCustomerOnly()
    {
        ViewBag.message = "This is for Customer only! Hi " + _userManager.GetUserName(HttpContext.User);
        return View("Views/Home/Index.cshtml");
    }

    [Authorize(Roles = "Seller")]
    public IActionResult ForSellerOnly()
    {
        ViewBag.message = "This is for Store Owner only!";
        return View("Views/Home/Index.cshtml");
    }
  ```

  3. Add this to the end of `Index.cshtml`
  ```html
  <p>
    @ViewBag.message
  </p>
  ```


  4. Run the app, try to login with the account (**II.1**) (this is a Customer, not a Seller).
  
  5. After logged in with (**II.1**), try to open page of Action `ForCustomerOnly`. OK ?

  6. Try to open page of Action `ForSellerOnly`. Is it "Access denied!" ?

  7. Similarly, log out, try again with (**II.2**) account.

