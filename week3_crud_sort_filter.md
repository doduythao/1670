# CRUD (Create, Read, Update, Delete) with Join, Top and Sort, Filtering.

## Prepare steps:

  - Just keep the week2ef project.
  - If got trouble with DB connection, check `week3_migration_update_ef.md`

## Basic Select with Join (and multiple join)

  We gonna extend the student/detail page to look like this

  ![extended image](https://docs.microsoft.com/en-us/aspnet/core/data/ef-mvc/crud/_static/student-details.png?view=aspnetcore-6.0)

  1. In `Controllers/StudentsController.cs`, the action method for the `Details`, modify current code to look like this

  ```c#
      var student = await _context.Students
        .Include(s => s.Enrollments)
            .ThenInclude(e => e.Course)
        .FirstOrDefaultAsync(m => m.ID == id);
  ```

  2. In `Views/Students/Detail.cshtml`, add below elements to the end of description list (after line `30`)

  ```c#
  <dt class="col-sm-2">
      @Html.DisplayNameFor(model => model.Enrollments)
  </dt>
  <dd class="col-sm-10">
      <table class="table">
          <tr>
              <th>Course Title</th>
              <th>Grade</th>
          </tr>
          @foreach (var item in Model.Enrollments)
          {
              <tr>
                  <td>
                      @Html.DisplayFor(modelItem => item.Course.Title)
                  </td>
                  <td>
                      @Html.DisplayFor(modelItem => item.Grade)
                  </td>
              </tr>
          }
      </table>
  </dd>
  ```

## Basic Count with Top and Offset.
  We gonna have pagination feature like this (TODO: `previous`, `next` buttons and limit the navigation band)

  ![count_offset_pagination](images/count_offset_pagination.png)

  0. Add this `private readonly int _recordsPerPage = 5;` after `private readonly SchoolContext _context;`

  1. Replace your current Index action by this code (in `StudentsController.cs`)

  ```c#
        // GET: Students
        public async Task<IActionResult> Index(int id = 0)
        {
            int numberOfRecords = await _context.Students.CountAsync();     //Count SQL
            int numberOfPages = (int)Math.Ceiling((double)numberOfRecords / _recordsPerPage);
            ViewBag.numberOfPages = numberOfPages;
            ViewBag.currentPage = id;
            List<Student> students = await _context.Students
                .Skip(id * _recordsPerPage)  //Offset SQL
                .Take(_recordsPerPage)       //Top SQL
                .ToListAsync();
            return View(students);
        }
  ```

  2. Add this pagination after the `table` in `Views/Students/Index.cshtml`

  ```html
    <div style="display:flex;text-align:center;justify-content:center">
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                @for (var i = 0; i < @ViewBag.numberOfPages; i++)
                {
                    var style = (ViewBag.currentPage == @i) ? "active" : "";
                    <li class="page-item @style">
                        <a class="page-link" asp-route-id="@i">@(i+1)</a>
                    </li>
                }
            </ul>
        </nav>
    </div>
  ```

## Add (Insert) one or more records to DB:
  
### We make `Create` action (HttpPost) better (Know when exception raised and How to handle exception)
  1. Drop the Bind for `ID` to avoid overposting attack
  2. Surround with try catch like this:

  ```c#
            try
            {
                if (ModelState.IsValid)
                {
                    _context.Add(student); //Add one right here.
                    //if you want to add more than one. Use this
                    //_context.AddRange(new List<Student>())
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (DbUpdateException ex)
            {
                ModelState.AddModelError("", "Unable to save changes. Error is: " + ex.Message);
            }
            return View(student);
  ```

## Update one or more records to DB:
  *A good way (more secure) is to fetch the existing one (or more) from DB again and on set the new value on them. Avoid updating changes directly by what you got from the form*

  So change the code of `HttpPost` `Edit` to this (*or just leave the code unchanged if you don't care the security problem*)
  ```c#
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,LastName,FirstMidName,EnrollmentDate")] Student student)
        {
            if (id != student.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var studentToUpdate = await _context.Students.FirstOrDefaultAsync(s => s.ID == id);
                if (studentToUpdate == null)
                {
                    return NotFound();
                }
                studentToUpdate.FirstMidName = student.FirstMidName;
                studentToUpdate.LastName = student.LastName;
                studentToUpdate.EnrollmentDate = student.EnrollmentDate;
                try
                {
                    _context.Update(studentToUpdate);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    ModelState.AddModelError("", "Unable to update the change. Error is: " + ex.Message);
                }
                return RedirectToAction(nameof(Index));
            }
            return View(student);
        }
  ```

## Delete a record or more records to DB
  *Again, we need log down what happened in case of errors go off*

  Change the code of `HttpPost` `DeleteConfirmed` to this:
  ```c#
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var student = await _context.Students.FindAsync(id);
            if (student == null)
            {
                return RedirectToAction(nameof(Index));
            }
            try
            {
                _context.Students.Remove(student);  //Delete
                //If you want to Delete more than one at a time. Try this
                //_contex.Student.RemoveRange(new List<Student>())
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (DbUpdateException ex)
            {
                ModelState.AddModelError("", "Unable to delete student " + id + ". Error is: " + ex.Message);
                return NotFound();
            }
        }
  ```

## Filtering (Where) the DB (more than just PK)

*Use one of these below lines of code based on what you wanna do*
```c#
List<Student> students = await _context.Students.Where(s => s.LastName=="Alexander").ToListAsync(); // where LastName ==
List<Student> students = await _context.Students.Where(s => s.LastName.StartsWith("a")).ToListAsync(); // where LastName Like
List<Student> students = await _context.Students.Where(s => EF.Functions.Like(s.LastName, "a%")).ToListAsync(); // similarly

List<Course> courses = await _context.Courses
                .Where(c => c.Credits > 2 && c.Title.EndsWith("economics")) // more than 1 condition
                .ToListAsync();
```

## Sorting the list on DB side (not on Server itself) And *another way to Query*

*We gonna make the column header to be sortable! (i.e. click the header to sort ASC or DESC)*
  ![count_offset_pagination](images/sorting.png)


  1. Replace the `Index` method in `StudentsController.cs` file with below code

  ```c#
    public async Task<IActionResult> Index(string sortOrder)
    {
        ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
        ViewData["DateSortParm"] = sortOrder == "Date" ? "date_desc" : "Date";
        var students = from s in _context.Students
                    select s;
        switch (sortOrder)
        {
            case "name_desc":
                students = students.OrderByDescending(s => s.LastName);
                break;
            case "Date":
                students = students.OrderBy(s => s.EnrollmentDate);
                break;
            case "date_desc":
                students = students.OrderByDescending(s => s.EnrollmentDate);
                break;
            default:
                students = students.OrderBy(s => s.LastName);
                break;
        }
        return View(await students.AsNoTracking().ToListAsync());
    }
  ```

  2. Replace the `<thead>` block in `Index.cshtml` by this new code:
  ```html
	<thead>
        <tr>
                <th>
                    <a asp-action="Index" asp-route-sortOrder="@ViewData["NameSortParm"]">@Html.DisplayNameFor(model => model.LastName)</a>
                </th>
                <th>
                    @Html.DisplayNameFor(model => model.FirstMidName)
                </th>
                <th>
                    <a asp-action="Index" asp-route-sortOrder="@ViewData["DateSortParm"]">@Html.DisplayNameFor(model => model.EnrollmentDate)</a>
                </th>
            <th></th>
        </tr>
    </thead>
  ```
## Try to make *Search box* like this image

  ![Search box](https://docs.microsoft.com/en-us/aspnet/core/data/ef-mvc/sort-filter-page/_static/filtering.png?view=aspnetcore-6.0)

  - Hint:
    - Query using this api: `students.Where(s => s.LastName.Contains(searchString))` 
    - Add this very small form as search box and search button
    ```html
    <form asp-action="Index" method="get">
        <div class="form-actions no-color">
            <p>
                Find by name: <input type="text" name="SearchString" value="@ViewData["CurrentFilter"]" />
                <input type="submit" value="Search" class="btn btn-default" /> |
                <a asp-action="Index">Back to Full List</a>
            </p>
        </div>
    </form>
    ```
